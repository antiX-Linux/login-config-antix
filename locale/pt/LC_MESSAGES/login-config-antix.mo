��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     w     �     �     �     �     �     �            !   %                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Acesso automático Mudar Mudar de gestor de acesso  Mudar a imagem de fundo Utilizador predefinido No acesso, activar o numlock Gestor de acesso Seleccionar tema Testar tema Testar o tema antes de o utilizar 