��          t      �         
             #     8     J     W     o     }  
   �     �  �  �  
   =     H     T     s     �  '   �     �     �     �  *   �                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2021
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Vetëhyrje Ndryshojeni Ndryshoni Përgjegjës Hyrjesh Ndryshoni sfondin Përdorues parazgjedhje Aktivizo tastin Nnumlock gjatë hyrjesh Përgjegjës Hyrjesh Përzgjidhni Temë Testoni Temën Testojeni temën, përpara se ta përdorni 