��          t      �         
             #     8     J     W     o     }  
   �     �    �  3   �     �  @        Q  1   m  \   �  -   �     *     B  C   ^                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Russian (https://www.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Автоматическая авторизация Изменить Изменение управления регистрацией Настройка фона Обыкновенный пользователь Активация цифровой клавиатуры при входе в систему Управление регистрацией Выберите вид Испытайте вида Испытание вида перед использованием 